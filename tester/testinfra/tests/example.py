#!/usr/bin/env python3
# -*- coding: UTF-8 -*-

'''
This script shows the basic structure for write a test class.
Convention names for script name and class name:
 - scritptest.py
 - Scriptest()
'''

from unittest import TestCase
# All tests functions are defineded in functions.py module
from defs import functions as f

class Example(TestCase):
    '''
    Class definition
    :params TestCase: module for write tests.
    '''
    # Method definition always have to start with: test_
    def test_pkgs(s):
        ''' Tests that checks if a list of packages are installed '''
        packages = ['package1', 'package2', 'package3']
        f.test_pkgs(s.host, packages, s.output)

    def test_services(s):
        ''' Tests that checks if a list of services are running and enabled '''
        services = ['service1', 'service2', 'service3']
        f.test_service_running(s.host, services, s.output)
        f.test_service_enabled(s.host, services, s.output)

    def test_ports(s):
        ''' Tests that checks if a list of ports are listening '''
        ports = ['127.0.0.1:3306/tcp', ':::80/tcp']
        f.test_ports(s.host, ports, s.output)

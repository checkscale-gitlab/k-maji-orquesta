#!/usr/bin/env python3
# -*- coding: UTF-8 -*-

from unittest import TestCase
from defs import functions as f

class Packer(TestCase):
    def test_file_exist(s):
        files = [
            '/usr/local/sbin/packer',
            '/kvm/isos',
            '/kvm/staging/server/images'
        ]
        f.test_file_exist(s.host, files, s.output)

    def test_pkgs(s):
        packages = ['qemu-utils']
        f.test_pkgs(s.host, packages, s.output)

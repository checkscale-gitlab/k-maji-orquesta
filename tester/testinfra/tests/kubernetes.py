#!/usr/bin/env python3
# -*- coding: UTF-8 -*-

from unittest import TestCase
from defs import functions as f

class Kubernetes(TestCase):
    def test_file_exist(s):
        files = ['/usr/bin/kubectl']
        f.test_file_exist(s.host, files, s.output)

# vagrant

## Int

You need vagrant to run this proyect on local using kvm hypervisor and libvirt.

## Req

We build this on ubuntu 20.04 desktop.

Install the follow list of packages:

- qemu-kvm 4.2-3
- libvirt-daemon-system 6.0.0
- libvirt-clients 6.0.0
- virt-manager 2.2.1
- vagrant 2.2.16

Build vagrant libvirt plugin:

$ CONFIGURE_ARGS='with-ldflags=-L/opt/vagrant/embedded/lib with-libvirt-include=/usr/include/libvirt with-libvirt-lib=/usr/lib' GEM_HOME=~/.vagrant.d/gems GEM_PATH=$GEM_HOME:/opt/vagrant/embedded/gems PATH=/opt/vagrant/embedded/bin:$PATH vagrant plugin install vagrant-libvirt

## Run

Start the local environment:

$ vagrant up --provider=libvirt

You can also set libvirt the default provider:

$ VAGRANT_DEFAULT_PROVIDER=libvirt

Show the status of the local environment:

$ vagrant status

Connect to deployer:

$ vagrant ssh deployer

## Clean

Clean this environment:

$ vagrant destroy -f


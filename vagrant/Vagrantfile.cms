# -*- mode: ruby -*-
# vi: set ft=ruby :

# Description: Vagrantfile for vagrant-fullvm-cms-devops project.
# Author: Armando Medina
# Email: jorge.medina@kronops.com.mx

Vagrant.configure(2) do |config|
  config.vm.define "cms" do |cms|
    config.env.enable # Enable vagrant-env(.env)
    config.proxy.http     = ENV['PROXY_URL']
    config.proxy.https    = ENV['PROXY_URL']

    cms.vm.hostname = "cms"
    cms.vm.network "private_network", ip: "192.168.33.4"
    cms.vm.network "forwarded_port", guest: 80, host: 1080
    cms.vm.synced_folder ".", "/vagrant", disabled: true
    cms.vm.provision "fix-no-tty", type: "shell" do |s|
      s.privileged = false
      s.inline = "sudo sed -i '/tty/!s/mesg n/tty -s \\&\\& mesg n/' /root/.profile"
    end
    cms.vm.provision :shell, name: "bin/bootstrap-node.sh", path: "bin/bootstrap-node.sh"
    cms.ssh.username = "vagrant"
    cms.ssh.password = "vagrant"
    cms.ssh.insert_key = true
  end

  config.vm.provider "virtualbox" do |vb|
    vb.customize ["modifyvm", :id, "--cpuexecutioncap", "30"]
    vb.memory = "512"
    vb.customize ["modifyvm", :id, "--cpus", "1"]
    vb.customize ["modifyvm", :id, "--ioapic", "on"]
  end
end

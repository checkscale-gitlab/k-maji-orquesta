postgresql-service
==================

Tasks for setting up PostgreSQL Server.

Requirements
------------

Ubuntu based system.

Role Variables
--------------

None.

Dependencies
------------

Not at the moment.

Example Playbook
----------------

To use this role, just include it in your playbook, for example:

    - hosts: servers
      roles:
         - postgresql-service

License
-------

MIT

Author Information
------------------

Please any question, please contact the autor at: jorge.medina@kronops.com.mx.

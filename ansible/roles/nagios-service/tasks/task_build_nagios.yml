---

- name: Clean nagios application root dir
  file: path=/usr/local/nagios/ state=absent

- name: Check nagios service status
  service: name=nagios state=started
  register: nagios_service
  ignore_errors: true

- name: Debug vars
  debug:
    var: nagios_service

- name: Stop nagios service
  service: name=nagios state=stopped
  when: nagios.state == "started"
  ignore_errors: true

- name: GET Nagios
  get_url: url={{ nagiosurl }} dest={{ download_dir }}/{{ nagiossrc }}.tar.gz

- name: Unpack Nagios source files
  shell: tar -xzvf {{ nagiossrc }}.tar.gz
  args:
    chdir: "{{ download_dir }}"
    creates: "{{ download_dir }}/{{ nagiossrc }}"

- name: Configure
  shell:  ./configure --with-command-group={{ monitoring_command_group }} \
                      --with-httpd_conf={{ with_httpd_conf }} \
                      --with-nagios-user={{ monitoring_user }}
  args:
    chdir: "{{ download_dir }}/{{ nagiossrc }}"
    creates: "{{ download_dir }}/{{ nagiossrc }}/Makefile"

- name: Make all
  shell: make all
  args:
    chdir: "{{ download_dir }}/{{ nagiossrc }}"
    creates: "{{ download_dir }}/{{ nagiossrc }}/base/nagios"

- name: Make install
  shell: make install
  args:
    chdir: "{{ download_dir }}/{{ nagiossrc }}"
    creates: /usr/local/nagios/bin/nagios

- name: Make install-config
  shell: make install-config
  args:
    chdir: "{{ download_dir }}/{{ nagiossrc }}"
    creates: /usr/local/nagios/etc/nagios.cfg

- name: Make install-commandmode
  shell: make install-commandmode
  args:
    chdir: "{{ download_dir }}/{{ nagiossrc }}"
    creates: /usr/local/nagios/var/rw

- name: Make install-devel
  shell: make install-devel
  args:
    chdir: "{{ download_dir }}/{{ nagiossrc }}"
    creates: /usr/local/nagios/include/nagios/nagios.h

- name: Make install-webconf
  shell: make install-webconf
  args:
    chdir: "{{ download_dir }}/{{ nagiossrc }}"
    creates: "{{ with_httpd_conf }}/nagios.conf"
  notify: restart apache

- name: Make install-init
  shell: make install-init
  args:
    chdir: "{{ download_dir }}/{{ nagiossrc }}"
    creates: /etc/init.d/nagios

- name: Set htpasswd for gui users
  htpasswd: path=/usr/local/nagios/etc/htpasswd.users name={{ item.user }} password={{ item.pass }} state=present
  with_items: "{{ nagios_users }}"

- name: Enable nagios servers path
  shell: sed -i '/\/usr\/local\/nagios\/etc\/servers/ s/^#//g' {{ nagios_cfg_path }}

- name: Create servers directory
  file: path="{{ nagios_servers_path }}" state=directory
        owner="{{ monitoring_user }}" group="{{ monitoring_user }}"

- name: Create eventhandlers log file
  file: path="/usr/local/nagios/var/eventhandlers.log" state=touch
        owner="{{ monitoring_user }}"
        group="{{ monitoring_user }}"

- name: Ensure Nagios is started
  service: name=nagios state=started enabled=yes

itop-service
============

Tasks for setting up itop service.

Requirements
------------

Ubuntu based system.

Role Variables
--------------

# These are the iTop database settings
itop_mysql_db: itopdb
itop_mysql_user: itopdbuser
itop_mysql_password: s3cr3t!!!

# Which version of iTop to deploy
itop_version: 2.2.0-2459
itop_sha256sum: 42ca594afc709cbef8528a6096f5a1efe96dcf3164e7ce321e87d57ae015cc82

Dependencies
------------

This role depends on the follow roles:

 * mysql-service

Example Playbook
----------------

To use this role, just include it in your playbook, for example:

    - hosts: servers
      roles:
        - itop-service

License
-------

MIT

Author Information
------------------

Please any question, please contact the autor at: jorge.medina@kronops.com.mx.
